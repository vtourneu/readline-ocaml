# OCaml bindings for GNU Readline

This library allows to use [GNU Readline](https://www.gnu.org/software/readline/)
in an OCaml project.

## Documentaiton

The [documentation is browsable online](https://acg.gitlabpages.inria.fr/dev/readline-ocaml/readline/index.html).

## Example

A [simple example](https://gitlab.inria.fr/vtourneu/readline-ocaml/-/blob/master/src/example.ml)
of the usage of this library is included with the source code.
It is a program which reads the input of the user using [readline], and writes
the same text on its output. It also save the input history in the file
`~/.readline-example_history`, and loads it when starting.
