#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>

#include <readline/readline.h>
#include <readline/history.h>

#define CAML_NAME_SPACE

#include "caml/mlvalues.h"
#include "caml/memory.h"
#include "caml/alloc.h"
#include "caml/callback.h"

static bool interrupted;
static bool catch_break = false;

static value caml_completion_function;

static int event(void)
{ return 0; }

static void handle_sigint(int signum, siginfo_t* siginfo, void* data)
{
	(void) signum;
	(void) siginfo;
	(void) data;

	interrupted = true;
	rl_done = 1;
}

static void set_sigact(struct sigaction* old_sigact)
{
	struct sigaction sigact;
	sigact.sa_sigaction = handle_sigint;
	sigemptyset(&(sigact.sa_mask));
	sigact.sa_flags = 0;
	sigaction(SIGINT, &sigact, old_sigact);
}

static void restore_sigact(struct sigaction* old_sigact)
{
	sigaction(SIGINT, old_sigact, NULL);
}

CAMLprim value fn_setup_catch_break(void)
{
	CAMLparam0();
	catch_break = true;
	CAMLreturn(Val_unit);
}

CAMLprim value fn_readline(value prompt)
{
	CAMLparam1(prompt);
	CAMLlocal1(result);

	const char* c_prompt = String_val(prompt);

	rl_event_hook = event;

	interrupted = false;

	struct sigaction old_sigact;
	if (catch_break)
		set_sigact(&old_sigact);
	char* line = readline(c_prompt);
	if (catch_break)
		restore_sigact(&old_sigact);

	if (interrupted)
	{
		free(line);
		CAMLreturn(caml_copy_string("i"));
	}
	if (line)
	{
		char* ret = malloc(sizeof (char) * (strlen(line) + 2));
		ret[0] = 'l';
		strcpy(ret + 1, line);
		free(line);
		result = caml_copy_string(ret);
		free(ret);
		CAMLreturn(result);
	}
	else
	{
		CAMLreturn(caml_copy_string("\n"));
	}
}

CAMLprim value fn_using_history(void)
{
	CAMLparam0();
	using_history();
	CAMLreturn(Val_unit);
}

CAMLprim value fn_add_history(value line)
{
	CAMLparam1(line);
	add_history(String_val(line));
	CAMLreturn(Val_unit);
}

CAMLprim value fn_read_history(value filename)
{
	CAMLparam1(filename);
	CAMLreturn(Val_int(read_history(String_val(filename))));
}

CAMLprim value fn_append_history(value nelements, value filename)
{
	CAMLparam2(nelements, filename);
	CAMLreturn(Val_int(append_history(Int_val(nelements), String_val(filename))));
}

CAMLprim value fn_append_last_entry_history(value line)
{
	CAMLparam1(line);
	const char* c_line = String_val(line);

	HIST_ENTRY* last = history_get(history_length);

	if (last)
	{
		int last_len = strlen(last->line);
		char* new_line = malloc(sizeof (char) * (last_len + strlen(c_line) + 1));
		strcpy(new_line, last->line);
		strcpy(new_line + last_len, c_line);
		free(last->line);
		last->line = new_line;
	}

	CAMLreturn(Val_unit);
}

CAMLprim value fn_get_last_entry_history(void)
{
	CAMLparam0();
	const HIST_ENTRY* last = history_get(history_length);
	if (last)
	{
		CAMLreturn(caml_copy_string(last->line));
	}
	else
	{
		CAMLreturn(caml_copy_string(""));
	}
}

char* custom_completer(const char* text, int state)
{
	CAMLparam0();
	CAMLlocal1(completion_list);

	static char** completions = NULL;
	static enum { FILENAMES, USERNAMES, CUSTOM } mode;

	if (state == 0)
	{
		if (completions)
			free(completions);

		completion_list = caml_callback2(caml_completion_function,
			caml_copy_string(rl_line_buffer), Val_int(rl_point));

		size_t completion_list_size = Wosize_val(completion_list);
		completions = malloc((completion_list_size + 1) * sizeof(char*));
		for (size_t i = 0; i < completion_list_size; i++)
		{
			const char* str = String_val(Field(completion_list, i));
			completions[i] = malloc(strlen(str) + 1);
			strcpy(completions[i], str);
		}
		completions[completion_list_size] = NULL;

		if (completion_list_size == 1 && strcmp(completions[0], "f") == 0)
			mode = FILENAMES;
		else if (completion_list_size == 1 && strcmp(completions[0], "u") == 0)
			mode = USERNAMES;
		else
			mode = CUSTOM;
	}

	switch (mode)
	{
		case FILENAMES:
			CAMLreturnT(char*, rl_filename_completion_function(text, state));
		case USERNAMES:
			CAMLreturnT(char*, rl_username_completion_function(text, state));
		case CUSTOM:
			CAMLreturnT(char*, completions[state + 1]);
	}
}

CAMLprim value fn_set_custom_completion_function(value completion_fun)
{
	CAMLparam1(completion_fun);

	static bool first_call = true;

	if (first_call)
	{
		first_call = false;

		rl_completion_entry_function = custom_completer;

		caml_completion_function = completion_fun;
		caml_register_generational_global_root(&caml_completion_function);
	}
	else
	{
		caml_modify_generational_global_root(&caml_completion_function,
			completion_fun);
	}

	CAMLreturn(Val_unit);
}

CAMLprim value fn_set_program_name(value name)
{
	CAMLparam1(name);

	const char* name_ocaml = String_val(name);

	char* name_buf = malloc(strlen(name_ocaml) + 1);
	strcpy(name_buf, name_ocaml);

	rl_readline_name = name_buf;

	CAMLreturn(Val_unit);
}

CAMLprim value fn_set_completion_append_character(value c)
{
	CAMLparam1(c);

	rl_completion_append_character = Int_val(c);

	CAMLreturn(Val_unit);
}

CAMLprim value fn_set_basic_word_break_characters(value sl)
{
	CAMLparam1(sl);

	const char* sl_ocaml = String_val(sl);

	static char* sl_buf = NULL;

	if (sl_buf)
	  free(sl_buf);

	sl_buf = malloc(strlen(sl_ocaml) + 1);
	strcpy(sl_buf, sl_ocaml);

	rl_basic_word_break_characters = sl_buf;

	CAMLreturn(Val_unit);
}
