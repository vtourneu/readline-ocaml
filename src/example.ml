let complete _str =
	Readline.Filenames

let rec loop () =
	try
		let str =
			Readline.readline
				~completion_fun:complete
				~prompt:"readline-example> "
				() in
		match str with
		| Some str ->
			(if str <> "" then
				Printf.printf "%s\n%!" str;
				Readline.add_history str);
			loop ()
		| None -> exit 0
	with
	| Sys.Break -> loop ()

let () =
	Readline.init
		~catch_break:true
		~program_name:"readline-example"
		~history_file:((Sys.getenv "HOME") ^ "/.readline-example_history")
		();
	loop ()
