#!/bin/sh

arg="$1"

system="$(uname)"

if [ "$system" = "Darwin" ]; then
	ireadlinedir="\"$(brew --prefix readline)/include\""
	lreadlinedir="\"-L$(brew --prefix readline)/lib\""
elif [ "$system" = "FreeBSD" ]; then
	ireadlinedir="/usr/local/include"
	lreadlinedir="-L/usr/local/lib"
else
	ireadlinedir=
	lreadlinedir=
fi

if [ "$arg" = "include" ]; then
	echo "($ireadlinedir)" > "iflags.sexp"
fi

if [ "$arg" = "lib" ]; then
	echo "($lreadlinedir -lreadline)" > "lflags.sexp"
fi
