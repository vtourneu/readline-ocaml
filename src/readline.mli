val init :
  ?history_file:string ->
  ?program_name:string ->
  ?catch_break:bool ->
  unit ->
  unit
(** This function must be called exactly once before any call to {!readline}.
    @param history_file Optional string containing the path to the history file.
      It will be created if it does not already exists. If absent, the history
      won't be saved at the end of the session.
    @param program_name Optional string containing the name of the application,
      which allows [libreadline] to perform conditional parsing of its config file.
    @param catch_break When set to [true], configure readline to emit the
      exception [exception:Sys.Break] when receiving the signal [SIGINT].
      Defaults to false. *)

type completion_result =
  | Filenames
  | Usernames
  | Custom of (string * char) list
  | Empty

val readline :
  ?completion_fun:(string -> completion_result) ->
  prompt : string ->
  unit ->
  string option
(** This function calls the [readline] function of [libreadline]. It returns
    a string containing the text inputted by the user, including the new line
    at the end, of [None] if the user didn't input any text (for example by
    sending [EOF] (ctrl+d) before inputting any text).
    @param completion_fun Optional completion function. It will be called
      when the user initiates a completion, with the content of the buffer up
      to the cursor position as argument. It may return:
      - {!Filenames} to indicate to perform a completion with filenames of the
        current directory. This uses [libreadline] built-in completion function
        for filenames.
      - {!Usernames} to indicate to perform a completion with usernames of the
        system. This uses [libreadline] built-in completion function
        for usernames.
      - {!Custom} with a list to indicate to perform a completion with the
        content of the list. Each element of the list should be of the form
        [(compl, suff)], [compl] beeing the completion alternative, and [suff]
        the inserted char when there is only one alternative.
      - {!Empty} to indicate that no completion should be performed. This is the
        default behaviour when no completion function is given.
    @param prompt String containing the prompt given to [libreadline], to display
      before waiting for input. *)

val set_word_break_chars : string -> unit
(** [set_word_break_chars char_list] sets the list of characters that [libreadline]
   uses to split the user input for the completion to [char_list]. The default
   value is [" \t\n\"\\'`@$><=;|&{("]. *)

val add_history : string -> unit
(** [add_history line] adds [line] in the [libreadline] history, in a new entry. *)

val append_to_last_entry : string -> unit
(** [append_to_last_entry line_cont] modifies the last entry of the history by
    appending [line_cont] at the end. Does nothing when the history is empty. *)

exception Not_initialized
(** This exception is thrown if any function except {!init} is called before
    {!init}. *)

exception Already_initialized
(** This exception is thrown if {!init} is called twice. *)

exception Can_t_read_history
(** This exception is thrown by {!init} when it fails to open the history file
    for read access. *)

exception Can_t_create_history
(** This exception is thrown by {!init} when it fails to create the history file. *)
