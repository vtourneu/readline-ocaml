external fn_readline : string -> string = "fn_readline"
external fn_using_history : unit -> unit = "fn_using_history"
external fn_add_history : string -> unit = "fn_add_history"
external fn_read_history : string -> int = "fn_read_history"
external fn_append_history : int -> string -> int = "fn_append_history"
external fn_append_last_entry_history : string -> unit = "fn_append_last_entry_history"
external fn_get_last_entry_history : unit -> string = "fn_get_last_entry_history"
external fn_set_custom_completion_function : (string -> int -> string array) -> unit = "fn_set_custom_completion_function"
external fn_set_program_name : string -> unit = "fn_set_program_name"
external fn_setup_catch_break : unit -> unit = "fn_setup_catch_break"
external fn_set_completion_append_character : char -> unit = "fn_set_completion_append_character"
external fn_set_basic_word_break_characters : string -> unit = "fn_set_basic_word_break_characters"

let nb_added_lines = ref 0
let init_successful = ref false
let history_filename = ref ""

exception Not_initialized
exception Already_initialized
exception Can_t_read_history
exception Can_t_create_history

type completion_result =
	| Filenames
	| Usernames
	| Custom of (string * char) list
	| Empty

let call_completion_fun completion_fun line pos =
	match completion_fun (String.sub line 0 pos) with
	| Filenames -> [| "f" |]
	| Usernames -> [| "u" |]
	| Custom l ->
		(match l with
		| [ (compl, c) ] ->
			let () = fn_set_completion_append_character c in
			[| "c" ; compl |]
		| _ -> Array.of_list ("c" :: (List.map fst l)))
	| Empty -> [| "c" |]

let readline ?completion_fun ~prompt () =
	if !init_successful then
		((match completion_fun with
		| Some completion_fun -> fn_set_custom_completion_function
			(call_completion_fun completion_fun)
		| _ -> fn_set_custom_completion_function (call_completion_fun (fun _ -> Empty)));
		let line = fn_readline prompt in
		match line.[0] with
		| 'i' -> raise Sys.Break
		| '\n' -> None
		| _ -> Some (String.sub line 1 ((String.length line) - 1)))
	else
		raise Not_initialized

let exit () =
	ignore (fn_append_history !nb_added_lines !history_filename)

let init ?history_file ?program_name ?(catch_break = false) () =
	if !init_successful then
		raise Already_initialized
	else
		let () = fn_using_history () in
		let () =
			match history_file with
			| Some history_file ->
				let () =
					match fn_read_history history_file with
					| 0 -> ()
					(* 2 means that the history file didn't exist, so we create it *)
					| 2 ->
						let () =
							try Out_channel.close (Out_channel.open_bin history_file)
							with _ -> raise Can_t_create_history in ()
					| _ -> raise Can_t_read_history in
				let () = history_filename := history_file in
				at_exit exit
			| None -> () in
		let () =
			match program_name with
			| Some program_name -> fn_set_program_name program_name
			| None -> () in
		let () =
			if catch_break then fn_setup_catch_break () in
		init_successful := true

let add_history line =
	if !init_successful then
		(if String.length line > 0 && line <> fn_get_last_entry_history () then
			(nb_added_lines := !nb_added_lines + 1;
			fn_add_history line))
	else
		raise Not_initialized

let append_to_last_entry str =
	if !init_successful then
		(if String.length str > 0 then fn_append_last_entry_history str)
	else
		raise Not_initialized

let set_word_break_chars str =
	fn_set_basic_word_break_characters str
