#!/bin/bash

set -e

ver="$CI_COMMIT_TAG"

if [ -n "$ver" ]; then
	files=$(git ls-files | grep -v "^\\.")
	tar="readline-${ver:1}.tar.gz"
	tar czf "$tar" $files
	curl --location --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
		--upload-file "$tar" \
		"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/releases/${ver}/$tar"
fi
